

def hello(greeting: str, name: str, *args):
    names = ', '.join([name] + list(args))
    print(f'{greeting} {names}!')


if __name__ == '__main__':
    hello('Hello', 'git', 'conflicts')

